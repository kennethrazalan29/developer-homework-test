import {
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";

import { NutrientFact, Product, Recipe } from "./supporting-files/models";
import {
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

export function sortNutrientFact(obj: { [key: string]: any }): {
  [key: string]: any;
} {
  const sortedKeys = Object.keys(obj).sort();
  const sortedObject = Object.fromEntries(
    sortedKeys.map((key) => [key, obj[key]])
  );

  return sortedObject;
}

// calculate the cheapest cost and nutrient facts for a recipe

function calculateCheapestCostAndNutrientFacts(recipe: Recipe) {
  let result = {
    cheapestCost: 0,
    nutrientsAtCheapestCost: {},
  };

  const lineItems = recipe.lineItems || [];

  for (const lineItem of lineItems) {
    // get all the matched products for the ingredient
    const matchedProducts = GetProductsForIngredient(lineItem.ingredient);

    // get cheapest cost and nutrient fact in base unit
    const nutrientFactAndCheapestCostInBaseUnit = matchedProducts.map(
      (product: Product) => {
        const nutrientFactInBaseUnits =
          product.nutrientFacts?.map((nutrientFact) =>
            GetNutrientFactInBaseUnits(nutrientFact)
          ) || [];

        const costPerBaseUnits =
          product.supplierProducts?.map((supplierProduct) =>
            GetCostPerBaseUnit(supplierProduct)
          ) || [];
        // console.log(costPerBaseUnits, "costPerBaseUnitscostPerBaseUnits");
        return {
          nutrientFactInBaseUnits,
          cheapestCostPerBaseUnit: Math.min(...costPerBaseUnits),
        };
      }
    );

    // use reduce to find out the cheapest cost
    const cheapestCost = nutrientFactAndCheapestCostInBaseUnit.reduce(
      (cheapestCost, cost) => {
        if (!cheapestCost) {
          return cost;
        }

        return cost.cheapestCostPerBaseUnit <
          cheapestCost.cheapestCostPerBaseUnit
          ? cost
          : cheapestCost;
      }
    );

    // merge cheapest cost per base unit
    result.cheapestCost +=
      cheapestCost.cheapestCostPerBaseUnit *
      (lineItem.unitOfMeasure?.uomAmount ?? 0);

    // merge nutrient fact quantity amount to the result
    cheapestCost.nutrientFactInBaseUnits.forEach((nutrientFact) => {
      if (
        !result.nutrientsAtCheapestCost.hasOwnProperty(
          nutrientFact.nutrientName
        )
      ) {
        result.nutrientsAtCheapestCost[nutrientFact.nutrientName] =
          nutrientFact;
        return;
      }
      const resultFact: NutrientFact =
        result.nutrientsAtCheapestCost[nutrientFact.nutrientName];
      resultFact.quantityAmount.uomAmount +=
        nutrientFact.quantityAmount.uomAmount;
    });

    // sort the the nutrient facts
    result.nutrientsAtCheapestCost = sortNutrientFact(
      result.nutrientsAtCheapestCost
    );
  }

  return result;
}

// loop to calculate the cheapest cost and nutrient facts on each recipe
for (const recipe of recipeData) {
  recipeSummary[recipe.recipeName] =
    calculateCheapestCostAndNutrientFacts(recipe);
}

RunTest(recipeSummary);
